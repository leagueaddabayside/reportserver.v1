/**
 * Created by aditya on 18/11/16.
 */
var codestring =
    {
        "merchant": {
            "webhookEndpointURL": "",
            "responseType": "",
            "responseEndpointURL": "",
            "description": "",
            "identifier": "T3239",
            "webhookType": ""
        },
        "cart": {
            "item": [
                {
                    "description": "Samsung Galaxy Duos New",
                    "providerIdentifier": "snapdeal.com",
                    "surchargeOrDiscountAmount": "0",
                    "amount": "5",
                    "comAmt": "0",
                    "sKU": "",
                    "reference": "test",
                    "identifier": "test"
                }
            ],
            "reference": "",
            "identifier": "",
            "description": ""
        },
        "payment": {
            "method": {
                "token": "470",
                "type": "N"
            },
            "instrument": {
                "expiry": {
                    "year": "",
                    "month": "",
                    "dateTime": ""
                },
                "provider": "",
                "iFSC": "",
                "holder": {
                    "name": "",
                    "address": {
                        "country": "",
                        "street": "",
                        "state": "",
                        "city": "",
                        "zipCode": "",
                        "county": ""
                    }
                },
                "bIC": "",
                "type": "",
                "action": "",
                "mICR": "",
                "verificationCode": "",
                "iBAN": "",
                "processor": "",
                "issuance": {
                    "year": "",
                    "month": "",
                    "dateTime": ""
                },
                "alias": "",
                "identifier": "",
                "token": "",
                "authentication": {
                    "token": "",
                    "type": "",
                    "subType": ""
                },
                "subType": "",
                "issuer": "",
                "acquirer": ""
            },
            "instruction": {
                "occurrence": "",
                "amount": "",
                "frequency": "",
                "type": "",
                "description": "",
                "action": "",
                "limit": "",
                "endDateTime": "",
                "identifier": "",
                "reference": "",
                "startDateTime": "",
                "validity": ""
            }
        },
        "transaction": {
            "deviceIdentifier": "Web",
            "smsSending": "N",
            "amount": 5,
            "forced3DSCall": "N",
            "type": "001",
            "description": "",
            "currency": "INR",
            "isRegistration": "Y",
            "identifier": "1487532004794",
            "dateTime": "09-02-2016",
            "token": "",
            "securityToken": "",
            "subType": "001",
            "requestType": "TKN",
            "reference": "ORD0001",
            "merchantInitiated": "N"
        },
        "consumer": {
            "mobileNumber": "9876543210",
            "emailID": "test@test.com",
            "identifier": "c9",
            "accountNo": ""
        }
    };
module.exports=codestring;
