'use strict';

module.exports = {

    mysql: {
        fantasyCricket: {
            host: '192.168.1.151',
            user: 'root',
            password: 'root',
            database: 'fantasy_cricket',
            connectionLimit: 20, //important
            debug: false
        }
    },
    redis: {
        host: '0.0.0.0',
        port: '6379'
    },
    mongo: {
        host: '192.168.1.151',
        port: '27017'
    },
    log: {
        format: 'dev',
        options: {
            stream: {
                directoryPath: process.cwd(),
                fileName: 'access.log',
                rotatingLogs: { // for more info on rotating logs - https://github.com/holidayextras/file-stream-rotator#usage
                    active: false, // activate to use rotating logs
                    fileName: 'access-%DATE%.log', // if rotating logs are active, this fileName setting will be used
                    frequency: 'daily',
                    verbose: false
                }
            }
        }
    },
    isProduction: false,
    serverUrl: 'http://192.168.1.151:3000', //http://www.thepokerbaazi.com   http://localhost:3000
    secure: {
        ssl: false,
        privateKey: './server.key',
        certificate: './server.crt'
    },
    serverAuth: {
        accessKey: 'mn@zaq@213',
        ip: '::ffff:127.0.0.1'
    },

};
