'use strict';

const express = require('./express');

module.exports.start = function start(callback) {
    var app = express.init();
    app.listen(5000, function () {
        console.log('listening on port : ' + 5000);
    });
};
