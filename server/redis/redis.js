"use strict";

const redis = require('../utils/redis_connection');
const tokenAuthPrefix = 'la_';
const adminTokenAuthPrefix = 'la_admin_';
const adminBannerPaneltokenAuthPrefix = 'la_banner_admin_';

const tokenValidity = 86400;   // in secods (1 day)
const adminTokenValidity = 1800; //in seconds
const responseCode =require('../utils/response_code');
const logger =require('../utils/logger').reportLogs;

const TokenizerClass = function() {}

TokenizerClass.prototype.checkToken = function (token) {
    return new Promise((resolve, reject)=> {
        let redisKey = tokenAuthPrefix + token;
        logger.trace('rediskey '+redisKey);
        redis.get(redisKey, function(err, savedData) {
            logger.trace('savedData',savedData);
            if (savedData){
                redis.expire(redisKey, tokenValidity);
                redis.expire(savedData, tokenValidity);
                return resolve(savedData);
                return;
            }
            reject(responseCode.INVALID_USER_TOKEN);
        });
    })
}

TokenizerClass.prototype.checkAdminToken = function (params) {
    let token = params.token;
    let redisKey = adminTokenAuthPrefix + token;
    if(params.adminType == 'BANNER_ADMIN'){
        redisKey = adminBannerPaneltokenAuthPrefix + token;
    }

    return new Promise((resolve, reject)=> {
        logger.trace('rediskey '+redisKey);
        redis.get(redisKey, function(err, savedData) {
            logger.trace('savedData',savedData);
            if (savedData){
                redis.hgetall(savedData, function(err, userData) {
                    if (userData){
                        redis.expire(redisKey, adminTokenValidity);
                        userData.token = token;
                        return resolve(userData);
                    }
                    reject(responseCode.INVALID_ADMIN_TOKEN);
                });
                return;
            }
            reject(responseCode.INVALID_ADMIN_TOKEN);
        });
    })
}

module.exports = new TokenizerClass();
/*---------------------------------------------*/
if (require.main === module) {
    (function () {

    })();
}