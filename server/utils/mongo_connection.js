var config = require('../../bin/config').mongo;
var logger = require("./logger").websiteLogs;
try {
    var mongoose = require('mongoose');
    var autoIncrement = require('mongoose-auto-increment');
    var connection = mongoose.connect('mongodb://'+config.host+'/fantasy_website');
    autoIncrement.initialize(connection);
} catch (err) {
    console.log(err);
}
var db = mongoose.connection;
db.on("error", function (err) {
    logger.error(err);
});