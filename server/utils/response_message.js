module.exports = {
	100 : 'Success',
	101 : 'Some unexpected error occurred',
	102 : 'Some unexpected error occurred',
	107 : 'Invalid admin token',
	108 :"Invalid user token",
	109 :"Invalid Request Type",
	110 : "User info not found",

	501:  "Mysql error",
	404:  "Not found"

}
