/**
 * Created by adityagupta on 20/1/17.
 */
const responseCode = require("./response_code.js");
const responseMessage = require("./response_message.js");

let utilities = function () {

};


/*
utilities.prototype.response = (params)=> {
    return {respCode: params.respCode, message: responseMessage[params.respCode], respData: params.data}
};
*/

utilities.prototype.response = (respCode,data)=> {
    return {respCode: respCode, message: responseMessage[respCode], respData: data}
};

module.exports = new utilities();
