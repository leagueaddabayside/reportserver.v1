"use strict";

var config = require('../../bin/config').redis;
var redis = require('redis');
var chalk = require('chalk');


function RedisClient() {
    if(config && config.host && config.port)
        return redis.createClient(config.port, config.host);
    return redis.createClient();
}

var RedisClient = new RedisClient();

RedisClient.on('error', function (err) {
    console.log(chalk.red('REDIS: Error ' + err));
});

var t = setTimeout(function() {
    console.log('REDIS: Failed to connect to ' + config.host + ':' + config.port);
    throw new Error('REDIS: Connection Failure');
}, 5000);

RedisClient.on('ready', function() {
    clearTimeout(t);
    console.log(chalk.green('REDIS is now connected'));
});

module.exports = RedisClient;
