/**
 * Created by sumit on 2/9/2017.
 */

var getAdminIdFromToken = require('./validate_admin_token');
var auditTrailLog = require('./audit_trail_log');

module.exports.getAdminIdFromToken = getAdminIdFromToken;
module.exports.auditTrailLog = auditTrailLog;
