/**
 * Created by sumit on 1/16/2017.
 */
const config = require('../../bin/config');
const dummy = require(config.__base + 'server/dummy');
const users = require(config.__base + 'server/users');
const accountTally = require(config.__base + 'server/account_tally');
const userCollection = require(config.__base + 'server/user_collection');

module.exports = function (app) {
    app.use('/', dummy);
    app.use('/', users);
    app.use('/reports/*', users);
    app.use('/', accountTally);
    app.use('/', userCollection);
};


 