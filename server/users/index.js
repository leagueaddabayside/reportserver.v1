/**
 * Created by sumit on 5/2/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();
let users = require('./controllers/index');
/* API CODE */
var middleware = require('../middleware/index');

route.post("/reports/user",middleware.auditTrailLog,middleware.getAdminIdFromToken, users.userInfoApi);
route.post("/reports/rake",middleware.auditTrailLog,middleware.getAdminIdFromToken, users.rakeReport);
route.get("/reports/bonus",middleware.auditTrailLog, users.bonusReport);
route.get("/reports/tds",middleware.auditTrailLog, users.tdsReport);

module.exports=route;
