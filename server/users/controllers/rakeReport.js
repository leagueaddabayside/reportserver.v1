/**
 * Created by adityagupta on 17/2/17.
 */
const rakeReportsClass=require('../services/rakeReport');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger =require('../../utils/logger').reportLogs;

const rakeReports=function (req,res) {
    var params=req.body;
    console.log(params);
    rakeReportsClass.getUserRakeReports(params)
        .then(function (result) {
            res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err,[]));
        })
};

module.exports=rakeReports;

if(require.main==module) {
    (function () {
        var req = {
            body: {
                matchId:72,
                tourId:9    ,
                rakeType:'DEBIT',
            }
        },res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        rakeReports(req,res);
    })()
}

