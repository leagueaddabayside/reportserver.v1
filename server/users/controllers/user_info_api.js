/**
 * Created by adityagupta on 17/2/17.
 */
const UserInfoClass=require('../services/user_info');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger =require('../../utils/logger').reportLogs;

const getUserReports=function (req,res) {
    let params=req.body;
    UserInfoClass.getUserReports(params)
        .then(function (result) {
            res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err,[]));
        })
};
module.exports=getUserReports;

if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:17,
                requestType:'docInfo'
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        getUserReports(req,res);
    })()
}
