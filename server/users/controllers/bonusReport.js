/**
 * Created by adityagupta on 17/2/17.
 */
const bonusReportsClass=require('../services/bonusReport');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger =require('../../utils/logger').reportLogs;
const Excel = require("exceljs");
const moment = require("moment");

const bonusReports=function (req,res) {
    var params=req.query;
    console.log(params);
    if(!params.startDate || !params.endDate){
        params.startDate=+new Date()/1000;
        params.endDate=+new Date()/1000;
    }
    bonusReportsClass.getUserBonusReports(params)
        .then(function (txns) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet("User Bonus Report");

            worksheet.columns = [
                {header: "Sr No", key: "srNo", width: 10},
                {header: "Transaction Id", key: "txn_id", width: 10},
                {header: "User Id", key: "user_id", width: 10},
                {header: "Team Name", key: "user_name", width: 10},
                {header: "Transaction Date", key: "txnDate", width: 10},
                {header: "Bonus Id", key: "bonus_id", width: 10},
                {header: "Amount", key: "amount", width: 32},
                {header: "remarks", key: "remarks", width: 10}
            ];
            for (var i=0, length=txns.length; i<length; i++) {
                var currentRow = txns[i];
                var currentObj = {};
                currentObj.srNo = i+1;
                currentObj.txn_id = currentRow.txn_id;
                currentObj.user_id = currentRow.user_id;
                currentObj.user_name = currentRow.screen_name;
                currentObj.txnDate = moment.unix(currentRow.txn_date).format("YYYY-DD-MM HH:mm:ss");
                currentObj.bonus_id = currentRow.bonus_id;
                currentObj.amount = currentRow.amount;
                currentObj.remarks = currentRow.remarks;

                worksheet.addRow(currentObj);
            }

            res.setHeader('Content-Type', 'application/vnd.openxmlformats');
            res.setHeader("Content-Disposition", "attachment; filename=" + "bonusReport.xlsx");
            workbook.xlsx.write(res).then(function () {
                logger.info("xls file is written.");

                res.end();
            });

            // res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err,[]));
        })
};

module.exports=bonusReports;

if(require.main==module) {
    (function () {
        var req = {
            body: {
                startDate:1493877292,
                endDate:Math.ceil(+new Date()/1000),
            }
        },res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        bonusReports(req,res);
    })()
}
