/**
 * Created by adityagupta on 17/2/17.
 */
const tdsReportClass = require('../services/tdsReport');
const Util = require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger = require('../../utils/logger').reportLogs;
const Excel = require("exceljs");
const moment = require("moment");

const tdsReport = function (req, res) {
    var params = req.query;

    params.startDate = moment.unix(params.startDate).format("YYYY-MM-DD");
    params.endDate = moment.unix(params.endDate).format("YYYY-MM-DD");

    tdsReportClass.getUserTdsReports(params)
        .then(function (txns) {
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet("User Bonus Report");

            worksheet.columns = [
                {header: "Sr No", key: "srNo", width: 10},
                {header: "User Id", key: "userId", width: 10},
                {header: "Team Name", key: "teamName", width: 10},
                {header: "Tds Date", key: "date", width: 10},
                {header: "Winning Id", key: "winningId", width: 10},
                {header: "Tds Amount", key: "tdsAmt", width: 32},
                {header: "Winning Amount", key: "winningAmt", width: 10}
            ];
            for (var i = 0, length = txns.length; i < length; i++) {
                var currentRow = txns[i];
                var currentObj = {};
                currentObj.srNo = i + 1;
                currentObj.userId = currentRow.userId;
                currentObj.teamName = currentRow.teamName;
                currentObj.date = moment(currentRow.date).format("YYYY-DD-MM HH:mm:ss");
                currentObj.winningId = currentRow.winningId;
                currentObj.tdsAmt = currentRow.tdsAmt;
                currentObj.winningAmt = currentRow.winningAmt;

                worksheet.addRow(currentObj);
            }

            res.setHeader('Content-Type', 'application/vnd.openxmlformats');
            res.setHeader("Content-Disposition", "attachment; filename=" + "tdsReport.xlsx");
            workbook.xlsx.write(res).then(function () {
                logger.info("xls file is written.");

                res.end();
            });

            // res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err, []));
        })
};

module.exports = tdsReport;

if (require.main == module) {
    (function () {
        var req = {
            query: {
                startDate: 1413877292,
                endDate: Math.ceil(+new Date() / 1000),
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        tdsReport(req, res);
    })()
}
