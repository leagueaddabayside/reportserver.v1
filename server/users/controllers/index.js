/**
 * Created by sumit on 5/2/2017.
 */
let userInfoApi = require('./user_info_api');
let rakeReport = require('./rakeReport');
let bonusReport = require('./bonusReport');
let tdsReport = require('./tdsReport');

module.exports.userInfoApi = userInfoApi;
module.exports.rakeReport = rakeReport;
module.exports.bonusReport = bonusReport;
module.exports.tdsReport = tdsReport;
