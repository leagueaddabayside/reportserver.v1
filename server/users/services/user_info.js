/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");
var utility = require("../../utils/utility");

function userInfoClass() {

}

userInfoClass.prototype.getUserReports = function (reqParam) {
    var that = this;
    logger.info("getUserReports reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }
                var param = [reqParam.userId];
                var query = '';
                switch (reqParam.requestType) {
                    case 'personalInfo':
                        query = "SELECT IFNULL(first_name,'') firstName,IFNULL(last_name,'') lastName,IFNULL(screen_name,'') teamName,is_mobile_verified mobileStatus,is_email_verified emailStatus,is_pan_card_verified panStatus,is_bank_detail_verified bankStatus,mobile_nbr mobileNo,email_id emailId FROM  gn_user_master um INNER JOIN gn_user_info AS info ON um.user_id=info.user_id  WHERE um.user_id=?";
                        break;
                    case 'docInfo':
                        query = "SELECT `bank_name` bankName,`account_number` accountNumber,`branch` branchName,`ifsc_code` ifscCode,`status`,ifnull(`remarks`,'')remarks FROM `gn_user_bank_details` WHERE user_id= ?";
                        break;
                    case 'panInfo':
                        query = "SELECT name,state,pan_number panNumber,`status`,ifnull(`remarks`,'')remarks FROM `gn_user_pan_card` WHERE user_id=?";
                        break;
                    case 'accountInfo':
                        query = "SELECT `deposit_amt` depositAmt,`bonus_amt` bonusAmt,`winning_amt` winningAmt,`last_deposit_date` lastDepositDate,`last_bonus_date` lastBonusDate,`total_winning` totalWinning,`win_leagues` leaguesWin FROM `gn_user_wallet_master` WHERE user_id=?";
                        break;
                    default:
                        return reject(responseCode.INVALID_REQUEST_TYPE);
                        break;
                }
                logger.info(query);
                // return;
                connection.query(query, param, function (err, rows, fields) {

                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    connection && connection.release();
                    if (rows && rows[0]) {
                        if (reqParam.requestType == 'docInfo') {
                            rows[0].accountNumber = that.beepCredential(rows[0].accountNumber)
                            rows[0].ifscCode = that.beepCredential(rows[0].ifscCode)
                        }else if (reqParam.requestType == 'personalInfo') {
                            if(rows[0].mobileNo){
                                rows[0].mobileNo = utility.encryptMobileNumber(rows[0].mobileNo)
                            }
                            if(rows[0].emailId){
                                rows[0].emailId = utility.encryptEmailId(rows[0].emailId)
                            }
                        }else if (reqParam.requestType == 'panInfo') {
                            if(rows[0].panNumber){
                                rows[0].panNumber = that.beepCredential(rows[0].panNumber)
                            }
                        }
                        logger.info("getUserPanCardInfo ,err, rows", param, err, rows);
                        return resolve(rows[0]);
                    } else {
                        return reject(responseCode.USER_RECORD_NOT_FOUND);
                    }

                });

            });
    });
}

userInfoClass.prototype.beepCredential = function (str) {
    if (typeof str !== 'string') str = str.toString();
    let leftPart = str.substring(0, Math.floor(str.length / 2));
    for (i = Math.ceil(str.length / 2); i <= str.length - 1; i++) {
        leftPart += 'X';
    }
    return leftPart;
};

module.exports = new userInfoClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getUserPanCardInfo({userId: 1})
}
