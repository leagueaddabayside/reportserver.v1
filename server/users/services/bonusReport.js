/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");

function bonusReportClass() {
 
}

bonusReportClass.prototype.getUserBonusReports = function (params) {
    return new Promise((resolve,reject)=>{
        fantayCricketPool.getConnection(function (err, connection) {
            if(err) {
                connection.release();
                return reject(responseCode.MYSQL_ERROR);
            }
            let query='select * from gn_user_real_bonus_txn as bonus join gn_user_master as info on ' +
                'bonus.user_id=info.user_id  where bonus.txn_date BETWEEN '+params.startDate+' AND '+params.endDate;
            connection.query(query, function (err, rows, fields) {
                console.log(err);
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }
                logger.info("rake reports ,err, rows", err, rows);
                connection && connection.release();

                return resolve(rows);
            });
        })

    })
};



module.exports = new bonusReportClass();
/**********************************/
if (require.main == module) {


}
