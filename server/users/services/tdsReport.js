/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");

function tdsReportClass() {

}

tdsReportClass.prototype.getUserTdsReports = function (params) {
    logger.info("getUserTdsReports reqParam", params);
    return new Promise((resolve,reject)=>{
        fantayCricketPool.getConnection(function (err, connection) {
            if(err) {
                connection.release();
                return reject(responseCode.MYSQL_ERROR);
            }
            var param = [params.startDate,params.endDate];
            let query='SELECT um.user_id userId,screen_name teamName,`date`,`winning_txn_id` winningId,`tds_amount` tdsAmt,`winning_amount` winningAmt FROM `gn_user_master` um INNER JOIN `gn_user_tds` tds ON um.user_id=tds.user_id WHERE date(date) BETWEEN ? AND ? ';

            connection.query(query,param, function (err, rows, fields) {
                console.log(err);
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }
                logger.info("tds reports ,err, rows", err, rows);
                connection && connection.release();

                return resolve(rows);
            });
        })

    })
};



module.exports = new tdsReportClass();
/**********************************/
if (require.main == module) {


}
