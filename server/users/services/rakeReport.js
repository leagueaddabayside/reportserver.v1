/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");

function rakeReportClass() {

}

rakeReportClass.prototype.getUserRakeReports = function (reqParam) {
    return new Promise((resolve,reject)=>{
        let tablename=(reqParam.rakeType.toUpperCase()=='CREDIT')?'gn_admin_rake_credit_txn':'gn_admin_rake_debit_txn';
        fantayCricketPool.getConnection(function (err, connection) {
            if(err) {
                connection.release();
                return reject(responseCode.MYSQL_ERROR);
            }
            let query='select txn_date,league_amt,league_id,rake_amt,config_id,winning_amt from '+tablename+' where tour_id=? and match_id=?';
            connection.query(query,[reqParam.tourId,reqParam.matchId], function (err, rows, fields) {
                console.log(err);
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }
                logger.info("rake reports ,err, rows", err, rows);
                connection && connection.release();

                return resolve(rows);
            });
        })
    })
};


module.exports = new rakeReportClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getUserPanCardInfo({userId: 1})
}
