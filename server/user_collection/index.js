/**
 * Created by sumit on 5/2/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();
let userCollection = require('./controllers/index');
/* API CODE */
var middleware = require('../middleware/index');

route.get("/getUserCollection", userCollection.getUserCollectionApi);
route.get("/getDateCollection", userCollection.getDateCollectionApi);

module.exports=route;