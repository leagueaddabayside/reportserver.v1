/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function dateCollectionClass() {

}

dateCollectionClass.prototype.getDateCollectionAmount = function (reqParam) {
    logger.info("getDateCollectionAmount reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = [reqParam.startDate,reqParam.endDate];
                var strQuery = "SELECT MAX(`txn_date`) txnDate ,SUM(`amount`)amount,`txn_type` txnType FROM `gn_user_real_txn_master` where txn_date between ? and ?  GROUP BY DATE(FROM_UNIXTIME(`txn_date`)),txn_type";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    logger.info("getDateCollectionAmount ,err, rows", param, err, rows);
                    connection && connection.release();

                    let userDataMap = new Map();
                    for (let i = 0, length = rows.length; i < length; i++) {
                        let currentRow = rows[i];
                        currentRow.txnDate = moment.unix(currentRow.txnDate).format("YYYY-MM-DD");
                        if (!userDataMap.has(currentRow.txnDate)) {
                            let defaultObj = {};
                            defaultObj.txnDate = currentRow.txnDate;
                            defaultObj.teamName = currentRow.teamName;
                            defaultObj.depositAmt = 0.0;
                            defaultObj.creditAmt = 0.0;
                            defaultObj.debitAmt = 0.0;
                            defaultObj.joinLeagueAmt = 0.0;
                            defaultObj.refundLeagueAmt = 0.0;
                            defaultObj.winningAmt = 0.0;
                            defaultObj.withdrawlAmt = 0.0;
                            defaultObj.withdrawlCancelAmt = 0.0;
                            defaultObj.bonusAmt = 0.0;
                            defaultObj.losingAmt = 0.0;
                            userDataMap.set(currentRow.txnDate, defaultObj);
                        }

                        let userObj = userDataMap.get(currentRow.txnDate);

                        var displayTxnType = '';
                        switch (currentRow.txnType) {

                            case 'CREDIT':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'DEBIT':
                                userObj.debitAmt = currentRow.amount;
                                break;
                            case 'DEPOSIT':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'JOIN_L':
                                userObj.joinLeagueAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'WITHDRAWL':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'WITHDRAWL_C':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'REFUND_L':
                                userObj.refundLeagueAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'WIN_L':
                                userObj.winningAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'BONUS':
                                userObj.bonusAmt = currentRow.amount;
                                break;

                            default:
                                logger.error('invalid txn type', currentRow);
                        }

                    }

                    return resolve([...userDataMap.values()]);
                });

            });
    });
}

dateCollectionClass.prototype.getDateCollectionCount = function (reqParam) {
    logger.info("getUserCollectionCount reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = [reqParam.startDate,reqParam.endDate];
                var strQuery = "SELECT MAX(`txn_date`) txnDate ,COUNT(1)count,`txn_type` txnType FROM `gn_user_real_txn_master` where txn_date between ? and ?  GROUP BY DATE(FROM_UNIXTIME(`txn_date`)),txn_type";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    logger.info("getUserCollectionAmount ,err, rows", param, err, rows);
                    connection && connection.release();

                    let userDataMap = new Map();
                    for (let i = 0, length = rows.length; i < length; i++) {
                        let currentRow = rows[i];
                        currentRow.txnDate = moment.unix(currentRow.txnDate).format("YYYY-MM-DD");
                        if (!userDataMap.has(currentRow.txnDate)) {
                            let defaultObj = {};
                            defaultObj.txnDate = currentRow.txnDate;
                            defaultObj.userId = currentRow.userId;
                            defaultObj.teamName = currentRow.teamName;
                            defaultObj.depositCount = 0;
                            defaultObj.creditCount = 0;
                            defaultObj.debitCount = 0;
                            defaultObj.joinLeagueCount = 0;
                            defaultObj.refundLeagueCount = 0;
                            defaultObj.winningCount = 0;
                            defaultObj.withdrawlCount = 0;
                            defaultObj.withdrawlCancelCount = 0;
                            defaultObj.bonusCount = 0;
                            defaultObj.losingCount = 0;
                            userDataMap.set(currentRow.txnDate, defaultObj);
                        }

                        let userObj = userDataMap.get(currentRow.txnDate);

                        var displayTxnType = '';
                        switch (currentRow.txnType) {

                            case 'CREDIT':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'DEBIT':
                                userObj.debitCount = currentRow.count;
                                break;
                            case 'DEPOSIT':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'JOIN_L':
                                userObj.joinLeagueCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);

                                break;
                            case 'WITHDRAWL':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'WITHDRAWL_C':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'REFUND_L':
                                userObj.refundLeagueCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);
                                break;
                            case 'WIN_L':
                                userObj.winningCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);
                                break;
                            case 'BONUS':
                                userObj.bonusCount = currentRow.count;
                                break;

                            default:
                                logger.error('invalid txn type', currentRow);
                        }

                    }

                    return resolve([...userDataMap.values()]);
                });

            });
    });
}


module.exports = new dateCollectionClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getDateCollectionAmount({matchId : 72})
}