/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");
const Excel = require("exceljs");

function userCollectionClass() {

}

userCollectionClass.prototype.getUserCollectionAmount = function (reqParam) {
    logger.info("getUserCollectionAmount reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = [reqParam.startDate,reqParam.endDate];
                var strQuery = "SELECT um.user_id userId,screen_name teamName,SUM(`amount`)amount,`txn_type` txnType FROM `gn_user_real_txn_master` rtm INNER JOIN `gn_user_master` um ON rtm.user_id=um.user_id where txn_date between ? and ? GROUP BY rtm.`user_id`,txn_type";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    logger.info("getUserCollectionAmount ,err, rows", param, err, rows);
                    connection && connection.release();

                    let userDataMap = new Map();
                    for (let i = 0, length = rows.length; i < length; i++) {
                        let currentRow = rows[i];

                        if (!userDataMap.has(currentRow.userId)) {
                            let defaultObj = {};
                            defaultObj.userId = currentRow.userId;
                            defaultObj.teamName = currentRow.teamName;
                            defaultObj.depositAmt = 0.0;
                            defaultObj.creditAmt = 0.0;
                            defaultObj.debitAmt = 0.0;
                            defaultObj.joinLeagueAmt = 0.0;
                            defaultObj.refundLeagueAmt = 0.0;
                            defaultObj.winningAmt = 0.0;
                            defaultObj.withdrawlAmt = 0.0;
                            defaultObj.withdrawlCancelAmt = 0.0;
                            defaultObj.bonusAmt = 0.0;
                            defaultObj.losingAmt = 0.0;
                            userDataMap.set(currentRow.userId, defaultObj);
                        }

                        let userObj = userDataMap.get(currentRow.userId);

                        var displayTxnType = '';
                        switch (currentRow.txnType) {

                            case 'CREDIT':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'DEBIT':
                                userObj.debitAmt = currentRow.amount;
                                break;
                            case 'DEPOSIT':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'JOIN_L':
                                userObj.joinLeagueAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'WITHDRAWL':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'WITHDRAWL_C':
                                userObj.creditAmt = currentRow.amount;
                                break;
                            case 'REFUND_L':
                                userObj.refundLeagueAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'WIN_L':
                                userObj.winningAmt = currentRow.amount;
                                userObj.losingAmt = userObj.joinLeagueAmt - (userObj.refundLeagueAmt + userObj.winningAmt);
                                break;
                            case 'BONUS':
                                userObj.bonusAmt = currentRow.amount;
                                break;

                            default:
                                logger.error('invalid txn type', currentRow);
                        }

                    }

                    return resolve([...userDataMap.values()]);
                });

            });
    });
}

userCollectionClass.prototype.getUserCollectionCount = function (reqParam) {
    logger.info("getUserCollectionCount reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = [reqParam.startDate,reqParam.endDate];
                var strQuery = "SELECT um.user_id userId,screen_name teamName,count(1)count,`txn_type` txnType FROM `gn_user_real_txn_master` rtm INNER JOIN `gn_user_master` um ON rtm.user_id=um.user_id where txn_date between ? and ?  GROUP BY rtm.`user_id`,txn_type";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    logger.info("getUserCollectionAmount ,err, rows", param, err, rows);
                    connection && connection.release();

                    let userDataMap = new Map();
                    for (let i = 0, length = rows.length; i < length; i++) {
                        let currentRow = rows[i];

                        if (!userDataMap.has(currentRow.userId)) {
                            let defaultObj = {};
                            defaultObj.userId = currentRow.userId;
                            defaultObj.teamName = currentRow.teamName;
                            defaultObj.depositCount = 0;
                            defaultObj.creditCount = 0;
                            defaultObj.debitCount = 0;
                            defaultObj.joinLeagueCount = 0;
                            defaultObj.refundLeagueCount = 0;
                            defaultObj.winningCount = 0;
                            defaultObj.withdrawlCount = 0;
                            defaultObj.withdrawlCancelCount = 0;
                            defaultObj.bonusCount = 0;
                            defaultObj.losingCount = 0;
                            userDataMap.set(currentRow.userId, defaultObj);
                        }

                        let userObj = userDataMap.get(currentRow.userId);

                        var displayTxnType = '';
                        switch (currentRow.txnType) {

                            case 'CREDIT':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'DEBIT':
                                userObj.debitCount = currentRow.count;
                                break;
                            case 'DEPOSIT':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'JOIN_L':
                                userObj.joinLeagueCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);

                                break;
                            case 'WITHDRAWL':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'WITHDRAWL_C':
                                userObj.creditCount = currentRow.count;
                                break;
                            case 'REFUND_L':
                                userObj.refundLeagueCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);
                                break;
                            case 'WIN_L':
                                userObj.winningCount = currentRow.count;
                                userObj.losingCount = userObj.joinLeagueCount - (userObj.refundLeagueCount + userObj.winningCount);
                                break;
                            case 'BONUS':
                                userObj.bonusCount = currentRow.count;
                                break;

                            default:
                                logger.error('invalid txn type', currentRow);
                        }

                    }

                    return resolve([...userDataMap.values()]);
                });

            });
    });
}


module.exports = new userCollectionClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getUserCollectionAmount({matchId : 72})
}