/**
 * Created by sumit on 5/2/2017.
 */
let getUserCollectionApi = require('./user_collection_report');
let getDateCollectionApi = require('./date_collection_report');

module.exports.getUserCollectionApi = getUserCollectionApi;
module.exports.getDateCollectionApi = getDateCollectionApi;
