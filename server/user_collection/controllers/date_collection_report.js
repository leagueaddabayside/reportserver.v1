/**
 * Created by adityagupta on 17/2/17.
 */
const dateCollectionClass = require('../services/date_collection_report');
const Util = require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger = require('../../utils/logger').reportLogs;
const Excel = require("exceljs");

const getDateCollection = function (req, res) {
    let params = req.query;

    if(params.dataType == 'AMOUNT'){
        dateCollectionClass.getDateCollectionAmount(params)
            .then(function (txns) {
                var workbook = new Excel.Workbook();
                var worksheet = workbook.addWorksheet("Date Amount Collection Report");

                worksheet.columns = [
                    {header: "Sr No", key: "srNo", width: 10},
                    {header: "Transaction Date", key: "txnDate", width: 10},
                    {header: "Deposit Amount", key: "depositAmt", width: 10},
                    {header: "Withdrawl Amount", key: "withdrawlAmt", width: 10},
                    {header: "Withdrawl Cancel Amount", key: "withdrawlCancelAmt", width: 10},
                    {header: "Bonus Amount", key: "bonusAmt", width: 10},
                    {header: "Credit Amount", key: "creditAmt", width: 10},
                    {header: "Debit Amount", key: "debitAmt", width: 10},
                    {header: "Join League Amount", key: "joinLeagueAmt", width: 10},
                    {header: "Refund Amount", key: "refundLeagueAmt", width: 10},
                    {header: "Winning Amount", key: "winningAmt", width: 10},
                    {header: "Losing Amount", key: "losingAmt", width: 10}
                ];
                for (var i=0, length=txns.length; i<length; i++) {
                    var currentRow = txns[i];
                    var currentObj = {};
                    currentObj.srNo = i+1;
                    currentObj.txnDate = currentRow.txnDate;
                    currentObj.depositAmt = currentRow.depositAmt;
                    currentObj.withdrawlAmt = currentRow.withdrawlAmt;
                    currentObj.withdrawlCancelAmt = currentRow.withdrawlCancelAmt;
                    currentObj.bonusAmt = currentRow.bonusAmt;
                    currentObj.creditAmt = currentRow.creditAmt;
                    currentObj.debitAmt = currentRow.debitAmt;
                    currentObj.joinLeagueAmt = currentRow.joinLeagueAmt;
                    currentObj.refundLeagueAmt = currentRow.refundLeagueAmt;
                    currentObj.winningAmt = currentRow.winningAmt;
                    currentObj.losingAmt = currentRow.losingAmt;

                    worksheet.addRow(currentObj);
                }

                res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                res.setHeader("Content-Disposition", "attachment; filename=" + "dateAmountCollectionReport.xlsx");
                workbook.xlsx.write(res).then(function () {
                    logger.info("xls file is written.");

                    res.end();
                });


                //res.json(Util.response(responseCode.SUCCESS, result));
            })
            .catch(function (err) {
                logger.error(err);
                res.json(Util.response(err, []));
            })
    }else{
        dateCollectionClass.getDateCollectionCount(params)
            .then(function (txns) {
                var workbook = new Excel.Workbook();
                var worksheet = workbook.addWorksheet("Date Count Collection Report");

                worksheet.columns = [
                    {header: "Sr No", key: "srNo", width: 10},
                    {header: "Transaction Date", key: "txnDate", width: 10},
                    {header: "Deposit Count", key: "depositCount", width: 10},
                    {header: "Withdrawl Count", key: "withdrawlCount", width: 10},
                    {header: "Withdrawl Cancel Count", key: "withdrawlCancelCount", width: 10},
                    {header: "Bonus Count", key: "bonusCount", width: 10},
                    {header: "Credit Count", key: "creditCount", width: 10},
                    {header: "Debit Count", key: "debitCount", width: 10},
                    {header: "Join League Count", key: "joinLeagueCount", width: 10},
                    {header: "Refund Count", key: "refundLeagueCount", width: 10},
                    {header: "Winning Count", key: "winningCount", width: 10},
                    {header: "Losing Count", key: "losingCount", width: 10}
                ];
                for (var i=0, length=txns.length; i<length; i++) {
                    var currentRow = txns[i];
                    var currentObj = {};
                    currentObj.srNo = i+1;
                    currentObj.txnDate = currentRow.txnDate;
                    currentObj.depositCount = currentRow.depositCount;
                    currentObj.creditCount = currentRow.creditCount;
                    currentObj.debitCount = currentRow.debitCount;
                    currentObj.joinLeagueCount = currentRow.joinLeagueCount;
                    currentObj.refundLeagueCount = currentRow.refundLeagueCount;
                    currentObj.winningCount = currentRow.winningCount;
                    currentObj.withdrawlCount = currentRow.withdrawlCount;
                    currentObj.withdrawlCancelCount = currentRow.withdrawlCancelCount;
                    currentObj.bonusCount = currentRow.bonusCount;
                    currentObj.losingCount = currentRow.losingCount;

                    worksheet.addRow(currentObj);
                }

                res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                res.setHeader("Content-Disposition", "attachment; filename=" + "dateCountCollectionReport.xlsx");
                workbook.xlsx.write(res).then(function () {
                    logger.info("xls file is written.");

                    res.end();
                });



               // res.json(Util.response(responseCode.SUCCESS, result));
            })
            .catch(function (err) {
                logger.error(err);
                res.json(Util.response(err, []));
            })
    }

}

module.exports = getDateCollection;

if (require.main == module) {
    (function () {
        var req = {
            body: {
                userId: 1
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        getDateCollection(req, res);
    })()
}
