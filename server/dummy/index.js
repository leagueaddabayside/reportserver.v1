/**
 * Created by sumit on 5/2/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();

/* API CODE */
route.get("/test", function (req, res) {
    var response = {};
    response.respCode = 100;
    response.respData = {};
    setTimeout(function(){
        res.json(response);
    }, 1000);
});

module.exports=route;