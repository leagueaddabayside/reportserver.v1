var config = {
	development : {
		//url to be used in link generation
		//for linux '/usr/lib/node_modules/'
		//for windows C:/Users/Shankar/AppData/Roaming/npm/node_modules/
		//NODE_MODULES_PATH : 'C:/Users/Shankar/AppData/Roaming/npm/node_modules/',
		NODE_MODULES_PATH : '',
		//NODE_MODULES_PATH : '/usr/lib/node_modules/',
		url : 'http://my.site.com',
		// protocol being ignored in main.js as of now - because both http and https listening has been enabled (if-else has been commented)
		protocol : 'https',
		connectionTimeout : 30000, //in ms
		memCacheTimeOut : 60000, //in ms
		apiMemCacheTimeOut : 86400000, // 24 hour in ms
		pokerListTimeOut : 10000, //in ms
		exeUrlTimeOut : 300000, //in ms
		handsCacheTimeOut : 50000, //in ms
		//mongodb connection settings
		cardplayDatabase : {
			host : 'localhost',
			user : 'gauss',
			password : '',
			database : 'cardplay',
			connectionLimit : 20, //important
			debug    :  false
		},
		cardplayPokerDatabase : {
			host : 'localhost',
			user : 'gauss',
			password : '',
			database : 'cardplay_poker',
			connectionLimit : 20, //important
			debug    :  false
		},
		//server details
                https_server : {
                        host : 'm.adda52.com',
                        port : '3333'
                },
                http_server : {
                        host : 'adda52.net',
                        port : '3334'
                }
	},
	production : {
		//url to be used in link generation
		url : 'http://my.site.com',
		//mongodb connection settings
		database : {
			host : '127.0.0.1',
			port : '27017',
			db : 'site'
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '3421'
		}
	}
};

module.exports = config;
