var mysql = require(NODE_PATH + 'mysql');
var config = require("./config.js");

module.exports = {
	cardPlayPokerPool : mysql.createPool(config.development.cardplayPokerDatabase),
	cardPlayPool : mysql.createPool(config.development.cardplayDatabase)
}
