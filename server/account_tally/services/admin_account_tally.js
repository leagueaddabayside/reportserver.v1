/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");

function adminAccountTallyClass() {

}

adminAccountTallyClass.prototype.getAdminRakeTally = function (reqParam) {
    logger.info("getAdminRakeTally reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = [reqParam.matchId, reqParam.matchId, reqParam.matchId, reqParam.matchId, reqParam.matchId];
                var strQuery = "SELECT match_id,SUM(join_amount) join_amount,SUM(rake_debit_amount) rake_debit_amount,SUM(winning_amount) winning_amount,SUM(rake_credit_amount) rake_credit_amount,SUM(tds_amount) tds_amount FROM (SELECT match_id,SUM(deposit_amt + `winning_amt` + `bonus_amt`)join_amount,0 winning_amount,0 rake_credit_amount,0 rake_debit_amount,0 tds_amount FROM `gn_user_real_legue_txn` WHERE STATUS='JOIN_LEAGUE' AND `match_id`= ? GROUP BY match_id UNION ALL SELECT match_id,0 join_amount,SUM(amount)winning_amount,0 rake_credit_amount,0 rake_debit_amount,0 tds_amount FROM `gn_user_real_winning_txn` WHERE match_id=? GROUP BY match_id UNION ALL SELECT match_id,0 join_amount,0 winning_amount,SUM(`rake_amt`)rake_credit_amount,0 rake_debit_amount,0 tds_amount FROM `gn_admin_rake_credit_txn` WHERE match_id= ? GROUP BY match_id UNION ALL SELECT match_id,0 join_amount,0 winning_amount,0 rake_credit_amount,-SUM(`rake_amt`)rake_debit_amount ,0 tds_amount FROM `gn_admin_rake_debit_txn` WHERE match_id= ? GROUP BY match_id UNION ALL SELECT match_id,0 join_amount,0 winning_amount,0 rake_credit_amount,0 rake_debit_amount,SUM(tds_amount)tds_amount FROM `gn_user_tds` tds INNER JOIN `gn_user_real_winning_txn` win ON tds.`winning_txn_id` = win.`txn_id` WHERE win.`match_id`= ? GROUP BY win.match_id)tlb GROUP BY match_id";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    //logger.info("getUserAccountTally ,err, rows", param, err, rows);
                    connection && connection.release();

                    let currentRow = rows[0];
                    //console.log('getAdminRakeTally',currentRow);

                    let rakeTallyRow = {};
                    rakeTallyRow.matchId = reqParam.matchId;
                    rakeTallyRow.joinAmt = 0;
                    rakeTallyRow.rakeDebitAmt = 0;
                    rakeTallyRow.rakeCreditAmt = 0;
                    rakeTallyRow.tdsAmt = 0;
                    rakeTallyRow.winningAmt = 0;
                    rakeTallyRow.tallyAmt = 0;
                    if (currentRow && currentRow.match_id) {
                        console.log('getAdminRakeTally',currentRow);
                        let tallyAmt = 0;
                        rakeTallyRow.joinAmt = currentRow.join_amount;
                        rakeTallyRow.rakeDebitAmt = currentRow.rake_debit_amount;
                        rakeTallyRow.rakeCreditAmt = currentRow.rake_credit_amount;
                        rakeTallyRow.tdsAmt = currentRow.tds_amount;
                        rakeTallyRow.winningAmt = currentRow.winning_amount;
                        tallyAmt = currentRow.join_amount + currentRow.rake_debit_amount - currentRow.tds_amount - currentRow.winning_amount - currentRow.rake_credit_amount;
                        rakeTallyRow.tallyAmt = tallyAmt.toFixed(2);
                    }

                    return resolve(rakeTallyRow);


                });

            });
    });
}


module.exports = new adminAccountTallyClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getAdminRakeTally({matchId: 72})
}