/**
 * Created by sumit on 5/2/2017.
 */
var fantayCricketPool = require("../../utils/mysql_connection").fantayCricketPool;
var logger = require("../../utils/logger").reportLogs;
var responseCode = require("../../utils/response_code");

function userAccountTallyClass() {

}

userAccountTallyClass.prototype.getUserAccountTally = function (reqParam) {
    logger.info("getUserPanCardInfo reqParam", reqParam);
    return new Promise((resolve, reject) => {
        fantayCricketPool
            .getConnection(function (err, connection) {
                if (err) {
                    logger.error(err);
                    connection && connection.release();
                    return reject(responseCode.MYSQL_ERROR);
                }

                var param = {};
                var strQuery = "SELECT um.user_id userId,um.screen_name teamName,expected_deposit expectedDeposit,expected_winning expectedWinning,expected_bonus expectedBonus,current_deposit currentDeposit,current_winning currentWinning,current_bonus currentBonus FROM(SELECT wallet.user_id,IFNULL(main.deposit_amt,0.0) expected_deposit,IFNULL(main.winning_amt,0.0) expected_winning,IFNULL(main.bonus_amt,0.0) expected_bonus,wallet.deposit_amt current_deposit,wallet.winning_amt current_winning,wallet.bonus_amt current_bonus FROM(SELECT user_id,SUM(`deposit_amt`)deposit_amt,SUM(`winning_amt`)winning_amt,SUM(`bonus_amt`)bonus_amt FROM(SELECT `user_id`,0 deposit_amt,0 winning_amt,SUM(`amount`)bonus_amt FROM `gn_user_real_bonus_txn` GROUP BY user_id UNION ALL SELECT `user_id`,SUM(`to_deposit`)deposit_amt,SUM(`to_winning`)winning_amt,SUM(`to_bonus`)bonus_amt FROM `gn_user_real_credit_txn` GROUP BY user_id UNION ALL SELECT `user_id`,SUM(`amount`)deposit_amt,0 winning_amt,0 bonus_amt FROM `gn_user_real_deposit_txn` GROUP BY user_id UNION ALL SELECT `user_id`,SUM(deposit_amt)deposit_amt,SUM(`winning_amt`)winning_amt,SUM(`bonus_amt`)bonus_amt FROM `gn_user_real_legue_txn` WHERE STATUS='CANCEL_LEAGUE' GROUP BY user_id UNION ALL SELECT `user_id`,SUM(`from_deposit`)deposit_amt,SUM(`from_winning`)winning_amt,SUM(`from_bonus`)bonus_amt FROM `gn_user_real_wihdrawal_txn` WHERE STATUS IN('CRM_C','ACC_C') GROUP BY user_id UNION ALL SELECT `user_id`,SUM(`deposit_amt`)deposit_amt,SUM(`winning_amt`)winning_amt,SUM(`bonus_amt`)bonus_amt FROM `gn_user_real_winning_txn` GROUP BY user_id UNION ALL SELECT `user_id`,-SUM(`from_deposit`)deposit_amt,-SUM(`from_winning`)winning_amt,-SUM(`from_bonus`)bonus_amt FROM `gn_user_real_debit_txn` GROUP BY user_id UNION ALL SELECT `user_id`,-SUM(deposit_amt)deposit_amt,-SUM(`winning_amt`)winning_amt,-SUM(`bonus_amt`)bonus_amt FROM `gn_user_real_legue_txn` GROUP BY user_id UNION ALL SELECT `user_id`,-SUM(`from_deposit`)deposit_amt,-SUM(`from_winning`)winning_amt,-SUM(`from_bonus`)bonus_amt FROM `gn_user_real_wihdrawal_txn` GROUP BY user_id)in_tlb GROUP BY user_id)main RIGHT JOIN (SELECT `user_id`,`deposit_amt`,`bonus_amt`,`winning_amt` FROM `gn_user_wallet_master`  WHERE (`deposit_amt` !=0 OR `bonus_amt` !=0 OR `winning_amt` !=0)) wallet ON main.user_id=wallet.user_id)tlb INNER JOIN gn_user_master um ON tlb.user_id=um.user_id WHERE expected_deposit != current_deposit OR expected_winning != current_winning OR expected_bonus != current_bonus";
                connection.query(strQuery, param, function (err, rows, fields) {
                    if (err) {
                        logger.error(err);
                        connection && connection.release();
                        return reject(responseCode.MYSQL_ERROR);
                    }
                    logger.info("getUserAccountTally ,err, rows", param, err, rows);
                    connection && connection.release();
                    return resolve(rows);
                });

            });
    });
}



module.exports = new userAccountTallyClass();
/**********************************/
if (require.main == module) {

    // var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: 123456});
    module.exports.getUserAccountTally({})
}