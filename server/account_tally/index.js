/**
 * Created by sumit on 5/2/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();
let accountTally = require('./controllers/index');
/* API CODE */
var middleware = require('../middleware/index');

route.post("/getUserAccountTally",middleware.auditTrailLog,middleware.getAdminIdFromToken, accountTally.getUserAccountTallyApi);
route.post("/getAdminRakeTally",middleware.auditTrailLog,middleware.getAdminIdFromToken, accountTally.getAdminRakeTallyApi);

module.exports=route;