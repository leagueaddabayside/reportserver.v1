/**
 * Created by adityagupta on 17/2/17.
 */
const AdminAccountTallyClass=require('../services/admin_account_tally');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger =require('../../utils/logger').reportLogs;

const getAdminRakeTallyApi=function (req,res) {
    let params=req.body;
    AdminAccountTallyClass.getAdminRakeTally(params)
        .then(function (result) {
            res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err,[]));
        })
}

module.exports=getAdminRakeTallyApi;

if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:1
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        getAdminRakeTallyApi(req,res);
    })()
}
