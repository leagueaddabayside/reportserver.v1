/**
 * Created by adityagupta on 17/2/17.
 */
const UserAccountTallyClass=require('../services/get_user_account_tally');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const logger =require('../../utils/logger').reportLogs;

const getUserAccountTallyApi=function (req,res) {
    let params=req.body;
    UserAccountTallyClass.getUserAccountTally(params)
        .then(function (result) {
            res.json(Util.response(responseCode.SUCCESS,result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(err,[]));
        })
}

module.exports=getUserAccountTallyApi;

if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:1
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        getUserAccountTallyApi(req,res);
    })()
}
