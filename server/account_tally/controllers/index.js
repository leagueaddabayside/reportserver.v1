/**
 * Created by sumit on 5/2/2017.
 */
let getUserAccountTallyApi = require('./get_user_account_tally');
let getAdminRakeTallyApi = require('./get_admin_rake_tally');

module.exports.getUserAccountTallyApi = getUserAccountTallyApi;
module.exports.getAdminRakeTallyApi = getAdminRakeTallyApi;